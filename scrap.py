import bs4 as bs
import urllib.request
import pandas as pd

#url to crawl
url = 'https://www.hltv.org/betting/money'
#pretend to be a browser
user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
headers = { 'User-Agent' : user_agent }
# 3 parameters (url, data, headers) data can be None in this case
req = urllib.request.Request(url, None, headers)

sauce = urllib.request.urlopen(req).read()
soup = bs.BeautifulSoup(sauce,'lxml')
#print(soup)

#for url in soup.find_all('a'):
#    print(url.get('href'))

#for div in soup.find_all('table', class_="bookmakerMatch"):
#    print(div.text)
#table = soup.find_all('table')
#print(table)

# <<table with pandas>>
dfs = pd.read_html(sauce)
#print(dfs[1])
#print(len(dfs))
#print(dfs[1].iat[0,1])
#print(len(dfs[1]))

#for df in dfs:
#    print(df)

#arbitrage function
def arber(odd1, odd2):
    bad = ['NaN', 'nan', 0]
    if (odd1 in bad) or (odd2 in bad):
        pass
    else:
        if ((1 / odd1) + (1 / odd2)) < 1:
            coeff = round((1 / odd1) + (1 / odd2), 3)
            #print('Yes', round((1 / odd1) + (1 / odd2), 3))
            #print(odd1)
            #print(odd2)

            #return variables to be used with apply_arber function
            return odd1, odd2, coeff
        else:
            #print('No')
            # return 0 or False
            return False

# appl arber on a dataframe
# feed values into arber function
def apply_arber(num):
    for i in range(1, 20):
        #print('========')
        #print('i = ', i)
        #print('========')
        for j in range(1, 20):
            #print("j = ", j)

            #if ,"(True)", function returns anything that means TRUE
            #else if function does not return anything = FALSE
            if arber(dfs[num].iat[0, i], dfs[num].iat[1, j]):
                #"unwrap" the tuple into the variables directly
                #https://www.mantidproject.org/Working_With_Functions:_Return_Values
                odd1, odd2, coeff = arber(dfs[num].iat[0, i], dfs[num].iat[1, j])
                cash_back(odd1, odd2, i, j, coeff)

#apply_arber(1)
# start- prevent index out of bound
# feed dataframes into apply_arber function
def start():
    i = 1
    final = len(dfs) - 1
    # skip first element with [1:]
    # because first row shows betting site data! not actuall matches
    for df in dfs[1:]:
        #print(df)
        print('Match '+ '#' + str(i))
        print(dfs[i].iat[0, 0] + ' vs ' + dfs[i].iat[1, 0])
        print(df)
        apply_arber(i)
        if i == final:
            break
        else:
            i += 1

def gains(odd1, odd2, stake, i, j, coeff):
    gain1 = (odd1 * stake)
    gain2 = (odd2 * (stake * (odd1 / odd2)))
    #ako je dobitak 0, skip
    if round(gain1 - round(stake + (stake * (odd1 / odd2)), 2)) == 0.00:
        pass
    else:
        #for the sake of printing in only 1 function and having order, we are draging 3 parameters
        #( coeff, i, j )
        #from apply_arber => cash_back => gains ... for the sake of this print looking nice
        #there MUST be a better way.
        print("=======================================")
        print('Yes, invers of sum of odds is = ', coeff)
        print('odd1 = ', odd1, 'Row 1 = ', i)
        print('odd2 = ', odd2, 'Row 2 = ', j)
        print("---------------------------------------")
        print("Win at ${} invested :=> ${}".format(round(stake, 2),round(gain1, 2)))
        print("Win at ${} invested :=> ${}".format(round((stake * (odd1 / odd2)), 2), round(gain2, 2)))
        print("Total invested ${}".format(round(stake + (stake * (odd1 / odd2)), 2)))
        print("Total won ${}".format(round(gain1 - (stake + (stake * (odd1 / odd2))), 2)))
        print("")


def cash_back(odd1, odd2, i, j, coeff):
    if odd1 < odd2:
        pass
    else:
        odd1, odd2 = odd2, odd1
    gains(odd1, odd2, 5, i, j, coeff)
    #gains(odd1, odd2, 10)
    #gains(odd1, odd2, 20)
    #gains(odd1, odd2, 50)

start()
